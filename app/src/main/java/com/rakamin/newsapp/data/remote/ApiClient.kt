package com.rakamin.newsapp.data.remote

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    const val BASE_URL = "https://newsapi.org"
    const val API_KEY = "2f60f54c4bb442a0996abe997509b7c5"


    //set interceptor
    private val logging: HttpLoggingInterceptor
        get() {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            return httpLoggingInterceptor.apply {
                this.level = HttpLoggingInterceptor.Level.BODY
            }
        }

    //create object client untuk retorfit
    private val client = OkHttpClient.Builder()
        .addInterceptor(logging)
        .build()


    val instanceTopHeadlines: ApiService by lazy {
        val retrofit = Retrofit.Builder().baseUrl("${BASE_URL}/v2/top-headlines/sources?apiKey=${API_KEY}")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        retrofit.create(ApiService::class.java)
    }



}