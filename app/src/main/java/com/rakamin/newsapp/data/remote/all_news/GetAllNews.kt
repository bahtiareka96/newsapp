package com.rakamin.newsapp.data.remote.all_news

data class GetAllNews(
    val sources: List<Source>,
    val status: String
)