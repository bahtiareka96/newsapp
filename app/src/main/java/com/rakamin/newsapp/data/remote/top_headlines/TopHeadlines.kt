package com.rakamin.newsapp.data.remote.top_headlines

import com.google.gson.annotations.SerializedName

data class TopHeadlines(
    @SerializedName("articles")
    val articles: List<Article>,
    @SerializedName("status")
    val status: String,
    @SerializedName("total_results")
    val totalResults: Int
)