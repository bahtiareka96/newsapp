package com.rakamin.newsapp.data.remote

import com.rakamin.newsapp.data.remote.all_news.GetAllNews
import com.rakamin.newsapp.data.remote.top_headlines.TopHeadlines
import com.rakamin.newsapp.data.remote.ApiClient.API_KEY
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import java.nio.channels.spi.AbstractSelectionKey
import java.util.Locale.IsoCountryCode

interface ApiService {

    @GET("v2/top-headlines")
    fun getTopHeadlines(
        @Query("country")
        countryCode: String = "us",
        @Query("page")
        pageNumber: Int = 1,
        @Query("apiKey")
        apiKey: String = API_KEY
    ): Call<TopHeadlines>

    @GET("v2/everything")
    suspend fun getAllNews(): GetAllNews
}