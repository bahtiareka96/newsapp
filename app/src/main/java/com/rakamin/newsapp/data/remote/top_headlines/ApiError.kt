package com.rakamin.newsapp.data.remote.top_headlines

import com.google.gson.annotations.SerializedName

data class ApiError(
    @SerializedName("code")
    val code: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)