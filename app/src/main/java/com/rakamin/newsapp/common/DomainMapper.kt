package com.rakamin.newsapp.common

interface DomainMapper<T,domainModel> {
    fun mapToDomainModel(modelDto:T) : domainModel
}