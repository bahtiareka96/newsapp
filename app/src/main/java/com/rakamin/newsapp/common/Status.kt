package com.rakamin.newsapp.common

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}