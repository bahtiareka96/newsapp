package com.rakamin.newsapp.domain.repository

import com.rakamin.newsapp.data.remote.ApiClient.instanceTopHeadlines
import com.rakamin.newsapp.data.remote.top_headlines.Article
import java.util.Locale.IsoCountryCode


class HomeRepo() {
    suspend fun getTopHeadlines(countryCode: String,pageNumber: Int) =
        instanceTopHeadlines.getTopHeadlines(countryCode,pageNumber)

}