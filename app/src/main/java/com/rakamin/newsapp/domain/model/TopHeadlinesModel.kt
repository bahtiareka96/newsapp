package com.rakamin.newsapp.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.rakamin.newsapp.data.remote.top_headlines.Article

@Entity
data class TopHeadlinesModel (
    @ColumnInfo(name = "articles") val articles : List<Article>,
    @ColumnInfo(name = "status") val status : String,
    @ColumnInfo(name = "total_results") val totalResult : Int,
)
