package com.rakamin.newsapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import com.rakamin.newsapp.data.remote.ApiClient
import com.rakamin.newsapp.databinding.ActivityMainBinding
import com.rakamin.newsapp.domain.repository.HomeRepo
import com.rakamin.newsapp.ui.newsApp.HomeViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
    }

}
