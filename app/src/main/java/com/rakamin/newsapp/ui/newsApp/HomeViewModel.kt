package com.rakamin.newsapp.ui.newsApp
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rakamin.newsapp.common.Resource
import com.rakamin.newsapp.data.remote.ApiClient
import com.rakamin.newsapp.data.remote.ApiService
import com.rakamin.newsapp.data.remote.top_headlines.TopHeadlines
import com.rakamin.newsapp.domain.model.TopHeadlinesModel
import com.rakamin.newsapp.domain.repository.HomeRepo
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.Locale.IsoCountryCode


class HomeViewModel(private val apiService: ApiService): ViewModel() {

    // di gunakan untuk assign value, hanya untuk di dalam view model
    private val _dataSuccess = MutableLiveData<TopHeadlines>()
    // di gunakan untuk observe di fragment/activity
    val dataSuccess : LiveData<TopHeadlines> get() = _dataSuccess

    private val _dataError = MutableLiveData<String>()
    val dataError : LiveData<String> get() = _dataError

    fun getTopHeadlines() {
        apiService.getTopHeadlines()
            .enqueue(object : Callback<TopHeadlines>{
                override fun onResponse(
                    call: Call<TopHeadlines>,
                    response: Response<TopHeadlines>
                ) {
                    // response.issuccessful sama dengan 200
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            _dataSuccess.postValue(response.body())
                        } else {
                            _dataError.postValue("Datanya kosong")
                        }
                    } else {
                        _dataError.postValue("Pengambilan data gagal}")
                    }
                    Log.e("onResponse", "test")
                }
                // kondisi get data gagal dari server/http, udh bener2 ga bisa di akses
                override fun onFailure(call: Call<TopHeadlines>, t: Throwable) {
                    _dataError.postValue("Server bermasalah")
                    Log.e("onFailure", "test")
                }

            })
    }


//    val topHeadlines: MutableLiveData<Resource<TopHeadlinesModel>> = MutableLiveData()
//    var topHeadlinesPage = 1
//
//    init {
//        getTopHeadlines("us")
//    }
//
//    fun getTopHeadlines(countryCode: String) = viewModelScope.launch {
//        topHeadlines.postValue(Resource.Loading())
//        val response = homeRepo.getTopHeadlines(countryCode, topHeadlinesPage)
//        topHeadlines.postValue(handleTopHeadlinesResponse(response))
//    }
//
//    private fun handleTopHeadlinesResponse(response: Response<TopHeadlinesModel>) : Resource<TopHeadlinesModel> {
//        if(response.isSuccessful) {
//            response.body()?.let { resultResponse ->
//                return Resource.Success(resultResponse)
//            }
//        }
//        return Resource.Error(response.message())
//    }
}