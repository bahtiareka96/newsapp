package com.rakamin.newsapp.ui.newsApp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.textview.MaterialTextView
import com.rakamin.newsapp.R
import com.rakamin.newsapp.data.remote.top_headlines.Article
import com.rakamin.newsapp.data.remote.top_headlines.TopHeadlines


class TopHeadlineAdapter: RecyclerView.Adapter<TopHeadlineAdapter.ArticleViewHolder>() {

    inner class ArticleViewHolder (itemView: View): RecyclerView.ViewHolder(itemView) {
        val urlToImage = itemView.findViewById<ShapeableImageView>(R.id.iv_headline_image)
        val source = itemView.findViewById<MaterialTextView>(R.id.tv_headline_source)
        val title = itemView.findViewById<MaterialTextView>(R.id.tv_headline_title)
        val description = itemView.findViewById<MaterialTextView>(R.id.tv_headline_description)
        val publishedAt = itemView.findViewById<MaterialTextView>(R.id.tv_headline_release)
        val itemclick = itemView.findViewById<MaterialCardView>(R.id.card_headline)
        fun bind(article: Article) {
            Glide.with(urlToImage).load(article.urlToImage).into(urlToImage)
            source.text = article.source.toString()
            title.text = article.title
            description.text = article.description
            publishedAt.text = article.publishedAt
            itemclick.setOnClickListener{
                onClick?.let { it(article) }
            }

        }
    }

    private val onClick:((Article)->Unit)? = null

    private val difCallback = object : DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, difCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_headline,parent,false)
        return ArticleViewHolder(view)

    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

}