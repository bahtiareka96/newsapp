package com.rakamin.newsapp.ui.newsApp

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.viewmodel.viewModelFactory
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import com.rakamin.newsapp.data.remote.ApiClient
import com.rakamin.newsapp.data.remote.ApiService
import com.rakamin.newsapp.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding!!
    lateinit var viewModel: HomeViewModel
    lateinit var topHeadlineAdapter: TopHeadlineAdapter
    private lateinit var viewPager2: ViewPager2
    companion object {
        fun newInstance() = HomeFragment()
    }

    private val apiService: ApiService by lazy { ApiClient.instanceTopHeadlines }
    lateinit var Adapter : TopHeadlineAdapter
    //private val viewModel: HomeViewModel by viewModelFactory { HomeViewModel(apiService) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        Adapter = TopHeadlineAdapter{Article ->
//            val bundle = Bundle()
//            bundle.putInt("id", id)
//            findNavController().navigate(R.id.action_homeFragment_to_movieDetailFragment, bundle)
//        }
        viewModel.getTopHeadlines()
        binding.rvListHeadline.layoutManager = LinearLayoutManager(requireContext())
        viewModel.dataSuccess.observe(viewLifecycleOwner){
            binding.rvListHeadline.adapter = Adapter
        }
        viewModel.dataError.observe(viewLifecycleOwner){
            Log.d("fail",it)
        }
    }






//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        _binding = FragmentHomeBinding.inflate(inflater,container,false)
//        return binding.root
//    }
//    override fun onDestroyView() {
//        super.onDestroyView()
//        _binding = null
//    }
//
//    val TAG = "Home"
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        viewModel = (activity as MainActivity).viewModel
//        setupRecyclerView()
//
//
//        viewModel.topHeadlines.observe(viewLifecycleOwner, Observer { response ->
//            when(response) {
//                is Resource.Success -> {
//                    hideProgressBar()
//                    response.data?.let { newsResponse ->
//                        topHeadlineAdapter.differ.submitList(newsResponse.articles)
//                    }
//                }
//                is Resource.Error -> {
//                    hideProgressBar()
//                    response.message?.let { message ->
//                        Log.e(TAG, "An error occured: $message")
//                    }
//                }
//                is Resource.Loading -> {
//                    showProgressBar()
//                }
//            }
//        })
//    }
//
//    private fun hideProgressBar() {
//        R.id.paginationProgressBar = View.INVISIBLE
//    }
//
//    private fun showProgressBar() {
//        R.id.paginationProgressBar = View.VISIBLE
//    }
//
//
//    private fun setupRecyclerView() {
//        topHeadlineAdapter =TopHeadlineAdapter()
//        R.id.rvListHeadline.apply {
//            adapter = topHeadlineAdapter
//            layoutManager = LinearLayoutManager(activity)
//        }
//    }
}